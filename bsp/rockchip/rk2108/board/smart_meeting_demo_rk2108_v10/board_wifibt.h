/**
  * Copyright (c) 2023 Rockchip Electronics Co., Ltd
  *
  * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __BOARD_WIFIBT_H__
#define __BOARD_WIFIBT_H__

#include "board_wifibt_base.h"

#define BT_GPIO_PORT                (GPIO_BANK0)
#define BT_POWER_GPIO_PIN           GPIO_PIN_D7
#define BT_GPIO_PORT_BASE           GPIO0

#endif
