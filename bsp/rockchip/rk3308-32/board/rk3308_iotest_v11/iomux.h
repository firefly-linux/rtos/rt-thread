/*
 * Copyright (c) 2022 Rockchip Electronics Co., Ltd.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-06-09     Cliff Chen   first implementation
 */

#ifndef __IOMUX_H__
#define __IOMUX_H__

#include "iomux_base.h"

void i2c0_m0_iomux_config(void);
void i2s0_2ch_m0_iomux_config(void);

#endif
